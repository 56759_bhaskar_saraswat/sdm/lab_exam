create table movie(movie_id int primary key auto_increment,movie_title varchar(50), movie_release_date DATE_FORMAT, movie_time TIME, director_name varchar(50));
insert into movie values (default,'DDLJ', '2000/05/12','03:00','Karan johar');
insert into movie values (default,'KKKG', '2001/06/04','03:00','Bhansali');
insert into movie values (default,'Rockstar', '2010/05/01','04:05','Imtiaz');
insert into movie values (default,'Chak de India', '2014/06/07','05:26','Kabir');
